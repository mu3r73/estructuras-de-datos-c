#include "agenda.hpp"

#include <stdexcept>
using namespace std;


Agenda::Agenda() {
  this->nextId = 1;
  this->personas = new Map<int, Persona>();
  this->indexApellido = new Map<string, List<Persona>>();
  this->indexTelefono = new Map<string, List<Persona>>();
}

Agenda::~Agenda() {
  delete(this->indexApellido);
  delete(this->indexTelefono);
  delete(this->personas);
}

int Agenda::getNewId() {
  return this->nextId++;
}

bool Agenda::tryGetById(int id, Persona *&persona) {
  return this->personas->tryGet(id, persona);
}

void Agenda::add(Persona *persona) {
  Persona *p;
  if (this->personas->tryGet(persona->getId(), p)) {
    throw runtime_error{"la persona ya está en la agenda"};
  }
  this->personas->addOrUpdate(persona->getId(), persona);

  // actualizo índice de apellidos
  this->addToIndex(this->indexApellido, persona->apellido, persona);
  
  // actualizo índice de teléfonos
  this->addToIndex(this->indexTelefono, persona->telefono, persona);
}

bool Agenda::tryRemove(int id) {
  Persona *p;
  if (this->personas->tryGet(id, p)) {
    this->personas->tryRemove(id);
    
    // borro persona del índice de apellidos
    this->removeFromIndex(this->indexApellido, p->apellido, p);
    
    // borrar persona del índice de teléfonos
    this->removeFromIndex(this->indexTelefono, p->telefono, p);
    
    return true;
    
  } else {
    return false;
  }
}

bool Agenda::tryUpdateNombre(int id, string nombre) {
  Persona *p;
  if (this->personas->tryGet(id, p)) {
    p->nombre = nombre;
    return true;
  } else {
    return false;
  }
}

bool Agenda::tryUpdateApellido(int id, string apellido) {
  Persona *p;
  if (this->personas->tryGet(id, p)) {
    string oldApellido = p->apellido;
    p->apellido = apellido;
    
    // actualizo índice de apellidos
    // borro persona de la entrada correspondiente al apellido viejo
    this->removeFromIndex(this->indexApellido, oldApellido, p);

    // agrego persona a la entrada correspondiente al apellido nuevo
    this->addToIndex(this->indexApellido, apellido, p);
    
    return true;
  } else {
    return false;
  }
}

bool Agenda::tryUpdateDireccion(int id, string direccion) {
  Persona *p;
  if (this->personas->tryGet(id, p)) {
    p->direccion = direccion;
    return true;
  } else {
    return false;
  }
}

bool Agenda::tryUpdateTelefono(int id, string telefono) {
  Persona *p;
  if (this->personas->tryGet(id, p)) {
    string oldTelefono = p->telefono;
    p->telefono = telefono;

    // actualizo índice de teléfonos
    // borro persona de la entrada correspondiente al teléfono viejo
    this->removeFromIndex(this->indexTelefono, oldTelefono, p);

    // agrego persona a la entrada correspondiente al teléfono nuevo
    this->addToIndex(this->indexTelefono, telefono, p);
    
    return true;
  } else {
    return false;
  }
}

void Agenda::addToIndex(Map<string, List<Persona>>* index, string key, Persona *persona) {
  List<Persona> *ps;
  if (!index->tryGet(key, ps)) {
    // no hay nadie con esa clave
    ps = new List<Persona>();
  }
  ps->append(persona);
  index->addOrUpdate(key, ps);
}

void Agenda::removeFromIndex(Map<string, List<Persona>>* index, string key, Persona *persona) {
  List<Persona> *ps;
  index->tryGet(key, ps);
  ps->remove(persona);
  
  // si no quedan personas con esa clave, elimino la entrada del índice
  if (ps->isEmpty()) {
    index->tryRemove(key);
  }
  //~ index->addOrUpdate(key, ps); // no es ne'sario
}

bool Agenda::tryGetByApellido(std::string apellido, List<Persona> *&personas) {
  return this->indexApellido->tryGet(apellido, personas);
}

bool Agenda::tryGetByTelefono(std::string telefono, List<Persona> *&personas) {
  return this->indexTelefono->tryGet(telefono, personas);
}

