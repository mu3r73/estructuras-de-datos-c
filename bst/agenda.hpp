#pragma once

#include "persona.hpp"
#include "map.hpp"
#include "list.hpp"

#include <string>


class Agenda {
private:
  int nextId;
  Map<int, Persona> *personas;
  Map<std::string, List<Persona>> *indexApellido;
  Map<std::string, List<Persona>> *indexTelefono;

public:
  // constructor - crea una agenda vacía
  Agenda();
  // destructor - libera memoria al borrar la agenda
  ~Agenda();
  // retorna una nueva identificación única para agendar una persona
  int getNewId();
  // si hay una persona con la identificación indicada en la agenda, la carga en persona, y retorna true;
  // si no, retorna false
  bool tryGetById(int id, Persona *&persona);
  // agrega una persona a la agenda;
  // si la persona ya estaba en la agenda, genera error
  void add(Persona *persona);
  // si hay una persona con la identificación indicada en la agenda, la borra, y retorna true;
  // si no, retorna false
  bool tryRemove(int id);
  // si hay una persona con la identificación indicada en la agenda, actualiza su nombre, y retorna true;
  // si no, retorna false
  bool tryUpdateNombre(int id, std::string nombre);
  // si hay una persona con la identificación indicada en la agenda, actualiza su apellido, y retorna true;
  // si no, retorna false
  bool tryUpdateApellido(int id, std::string apellido);
  // si hay una persona con la identifiación indicada en la agenda, actualiza su dirección, y retorna true;
  // si no, retorna false
  bool tryUpdateDireccion(int id, std::string direccion);
  // si hay una persona con la identificación indicada en la agenda, actualiza su teléfono, y retorna true;
  // si no, retorna false
  bool tryUpdateTelefono(int id, std::string telefono);
  // si personas con el apellido indicado en la agenda, las cargas en personas, y retorna true;
  // si no, retorna false
  bool tryGetByApellido(std::string apellido, List<Persona> *&personas);
  // si personas con el teléfono indicado en la agenda, las cargas en personas, y retorna true;
  // si no, retorna false
  bool tryGetByTelefono(std::string telefono, List<Persona> *&personas);

private:
  // agrega la persona al índice, con clave key
  void addToIndex(Map<std::string, List<Persona>>* index, std::string key, Persona *persona);
  // borra la persona del índice, con clave key
  void removeFromIndex(Map<std::string, List<Persona>>* index, std::string key, Persona *persona);
};

