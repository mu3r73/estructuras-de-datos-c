#pragma once

#include "list.hpp"


// nodo de Map
template<typename TKey, typename TValue>
struct MNode {
  TKey key;
  TValue *value;
  MNode<TKey, TValue>* left;
  MNode<TKey, TValue>* right;
  
  ~MNode();
};

// Map genérico, usando BST
template<typename TKey, typename TValue>
class Map {
private:
  MNode<TKey, TValue>* root;
  int length;

public:
  // constructor - crea un árbol vacío
  Map();
  // destructor - libera la memoria al borrar el árbol
  ~Map();
  // indica si el árbol está vacío
  bool isEmpty();
  // si existe un nodo con la clave indicada, reemplaza su valor asociado;
  // si no existe un nodo con la clave indicada, lo agrega y le asocia el valor
  void addOrUpdate(TKey key, TValue *value);
  // si existe un nodo con la clave indicada, carga su valor en value, y retorna true;
  // si no existe un nodo con la clave indicada, retorna false
  bool tryGet(TKey key, TValue *&value);
  // si existe un nodo con la clave indicada, lo elimina del árbol, y retorna true;
  // si no existe un nodo con la clave indicada, retorna false
  bool tryRemove(TKey key);
  // devuelve la cantidad de nodos del árbol
  int getLength();
  // retorna una lista con las claves del árbol (inOrder), para debugging
  List<TKey>* keys();


private:
  // addOrUpdate recursivo, a partir del nodo indicado
  MNode<TKey, TValue>* addOrUpdateRec(MNode<TKey, TValue> *node, TKey key, TValue *value);
  // tryGet recursivo, a partir del nodo indicado
  MNode<TKey, TValue>* tryGetRec(MNode<TKey, TValue> *node, TKey key);
  // tryRemove recursivo, a partir del nodo indicado
  MNode<TKey, TValue>* tryRemoveRec(MNode<TKey, TValue> *node, TKey key);
  // reconstruye un (sub)árbol, a partir de los nodos left, right dados
  MNode<TKey, TValue>* rebuildSubtree(MNode<TKey, TValue> *left, MNode<TKey, TValue> *right);
  // retorna el nodo con key máxima a partir del nodo indicado
  MNode<TKey, TValue>* getMax(MNode<TKey, TValue> *node);
  // keys recursivo
  void keysRec(List<TKey> *res, MNode<TKey, TValue> *node);
};

