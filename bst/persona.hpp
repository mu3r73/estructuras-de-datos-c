#pragma once

#include <ostream>


// datos de una persona
struct Persona {
  std::string nombre;
  std::string apellido;
  std::string direccion;
  std::string telefono;
  
public:
  // constructor - crea una nueva persona, con los datos indicados
  Persona(int id, std::string nombre, std::string apellido, std::string direccion, std::string telefono);
  // retorna el ID de la persona
  int getId();
  // overload de ==
  bool operator==(Persona &persona);
  // overload de << para debugging
  friend std::ostream &operator<<(std::ostream &out, Persona &persona);

private:
  int id;
};

