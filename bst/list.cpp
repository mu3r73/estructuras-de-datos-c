#include "list.hpp"

#include <stdexcept>
using namespace std;


template<typename T>
LNode<T>::~LNode() {
  //~ if (this->value != nullptr) {
    //~ delete(this->value);
    //~ this->value = nullptr;
  //~ }
}

template<typename T>
List<T>::List() {
  this->first = nullptr;
  this->last = nullptr;
  this->length = 0;
}

template<typename T>
List<T>::~List() {
  while (!this->isEmpty()) {
    this->pop();
  }
}

template<typename T>
bool List<T>::isEmpty() {
  return this->length == 0;
}

template<typename T>
T* List<T>::head() {
  if (this->isEmpty()) {
    throw runtime_error{"lista vacía"};
  }
  return this->first->value;
}

template<typename T>
void List<T>::append(T *value) {
  LNode<T>* node = new LNode<T>();
  node->value = value;
  node->next = nullptr;
  if (this->isEmpty()) {
    this->first = node;
  } else {
    this->last->next = node;
  }
  this->last = node;
  this->length++;
}

template<typename T>
void List<T>::pop() {
  if (this->isEmpty()) {
    throw runtime_error{"lista vacía"};
  }
  LNode<T>* node = this->first;
  this->first = node->next;
  delete(node);
  this->length--;
}

template<typename T>
void List<T>::remove(T *value) {
  if (this->isEmpty()) {
    // lista vacía => nada que borrar
    return;
  }
  if (this->first->value == value) {
    // borrar primer elemento de la lista
    LNode<T> *newHead = this->first->next;
    delete(this->first);
    this->length--;
    this->first = newHead;
    return;
  }
  // borrar elemento no-primero de la lista
  removeRec(this->first, value);
}

template<typename T>
void List<T>::removeRec(LNode<T> *node, T *value) {
  LNode<T> *next = node->next;
  if (next == nullptr) {
    // el elemento no está en la lista
    return;
  }
  // node no es vacío
  if (next->value == value) {
    // el elemento a borrar es el siguiente
    node->next = next->next;
    if (node->next == nullptr) {
      // borrar último elemento
      this->last = node;
    }
    delete(next);
    this->length--;
  } else {
    // el elemento a borrar no es el siguiente
    removeRec(next, value);
  }
}

template<typename T>
int List<T>::getLength() {
  return this->length;
}

template<typename T>
ListIterator<T>* List<T>::getIterator() {
  return new ListIterator<T>(this->first);
}

template<typename T>
ListIterator<T>::ListIterator(LNode<T>* first) {
  this->current = first;
}

template<typename T>
bool ListIterator<T>::hasNext() {
  return this->current != nullptr;
}

template<typename T>
T* ListIterator<T>::next() {
  T* res = this->current->value;
  this->current = this->current->next;
  return res;
}

