#pragma once


// nodo de lista genérica
template<typename T>
struct LNode {
  T *value;
  LNode<T> *next;
  
  // destructor
  ~LNode();
};

template<typename T>
class ListIterator;

// lista genérica
template<typename T>
class List {
private:
  LNode<T>* first;
  LNode<T>* last;
  int length;

public:
  // constructor - crea una lista vacía
  List();
  // destructor - libera la memoria al borrar la lista
  ~List();
  // indica si la lista está vacía
  bool isEmpty();
  // retorna el primer elemento de la lista;
  // si la lista es vacía, genera error
  T* head();
  // agrega un elemento al final de la lista
  void append(T *value);
  // elimina el primer elemento de la lista
  // si la lista es vacía, genera error
  void pop();
  // elimina (la primera instancia d)el elemento indicado de la lista
  void remove(T *value);
  // retorna la longitud de la lista
  int getLength();
  // crea una 'copia' de la lista en list
  ListIterator<T>* getIterator();

private:
  // remove recursivo, a partir del nodo indicado
  void removeRec(LNode<T> *node, T *value);
};

// iterador para la lista genérica
template<typename T>
class ListIterator {
private:
  LNode<T>* current;

public:
  ListIterator(LNode<T>* first);
  bool hasNext();
  T* next();
};

