#pragma once

#include "persona.hpp"

#include <ostream>
using namespace std;


Persona::Persona(int id, string nombre, string apellido, string direccion, string telefono) {
  this->id = id;
  this->nombre = nombre;
  this->apellido = apellido;
  this->direccion = direccion;
  this->telefono = telefono;
}

int Persona::getId() {
  return this->id;
}

bool Persona::operator==(Persona &persona) {
  return (this->id == persona.id);
}

ostream &operator<<(ostream &out, Persona &persona) {
  out << persona.getId() << " - " << persona.apellido << ", " << persona.nombre
      << " - " << persona.direccion << " - " << persona.telefono;
  return out;
}

