#include "agenda.cpp"
#include "map.cpp"
#include "list.cpp"
#include "persona.cpp"

#include <iostream>
#include <string>
using namespace std;


void clearCin() {
  char c;
  while (cin.get(c) && c != '\n') {
    // descarto residuos en cin
  }
}

string leerString(string label, int length) {
  cout << label << ": ";
  char res[length];
  cin.getline(res, sizeof(res));
  return res;
}

void agregarPersona(Agenda *agenda) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> agregar persona" << endl << endl;

  clearCin();
  string apellido = leerString("apellido", 30);
  string nombre = leerString("nombre", 40);
  string direccion = leerString("dirección", 40);
  string telefono = leerString("teléfono", 20);
  
  Persona *p = new Persona(agenda->getNewId(), nombre, apellido, direccion, telefono);
  agenda->add(p);
  cout << endl << *p << endl << endl;
}

void modificarApellido(Agenda *agenda, Persona *p) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> modificar apellido" << endl;
  cout << *p << endl << endl;
  
  clearCin();
  string apellido = leerString("ingresá el nuevo apellido", 30);
  cout << endl;
  
  if (agenda->tryUpdateApellido(p->getId(), apellido)) {
    cout << "se modificó el apellido" << endl;
  } else {
    cout << "no se pudo modificar el apellido" << endl;
  }
  cout << endl << *p << endl << endl;
}

void modificarNombre(Agenda *agenda, Persona *p) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> modificar nombre" << endl;
  cout << *p << endl << endl;
  
  clearCin();
  string nombre = leerString("ingresá el nuevo nombre", 40);
  cout << endl;
  
  if (agenda->tryUpdateNombre(p->getId(), nombre)) {
    cout << "se modificó el nombre" << endl;
  } else {
    cout << "no se pudo modificar el nombre" << endl;
  }
  cout << endl << *p << endl << endl;
}

void modificarDireccion(Agenda *agenda, Persona *p) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> modificar dirección" << endl;
  cout << *p << endl << endl;
  
  clearCin();
  string direccion = leerString("ingresá la nueva dirección", 40);
  cout << endl;
  
  if (agenda->tryUpdateDireccion(p->getId(), direccion)) {
    cout << "se modificó la dirección" << endl;
  } else {
    cout << "no se pudo modificar la dirección" << endl;
  }
  cout << endl << *p << endl << endl;
}

void modificarTelefono(Agenda *agenda, Persona *p) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> modificar teléfono" << endl;
  cout << *p << endl << endl;
  
  clearCin();
  string telefono = leerString("ingresá el nuevo teléfono", 20);
  cout << endl;
  
  if (agenda->tryUpdateTelefono(p->getId(), telefono)) {
    cout << "se modificó el teléfono" << endl;
  } else {
    cout << "no se pudo modificar el teléfono" << endl;
  }
  cout << endl << *p << endl << endl;
}

void borrarPersona(Agenda *agenda, Persona *p) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> borrar persona" << endl;
  cout << *p << endl << endl;
  
  if (agenda->tryRemove(p->getId())) {
    cout << "se borró la persona de la agenda" << endl;
  } else {
    cout << "no se pudo borrar la persona de la agenda" << endl;
  }
}

void submenuModificarOBorrarPersona(Agenda *agenda, Persona *p) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> modificar o borrar persona" << endl;
  cout << *p << endl << endl;
  
  cout << "opciones:" << endl;
  cout << "1. modificar apellido" << endl;
  cout << "2. modificar nombre" << endl;
  cout << "3. modificar dirección" << endl;
  cout << "4. modificar teléfono" << endl;
  cout << "5. borrar persona" << endl;
  cout << "0. salir" << endl << endl;
  cout << "opción: ";
  
  char c;
  cin >> c;
  switch(c) {
    case '0':
      break;
    case '1':
      modificarApellido(agenda, p);
      break;
    case '2':
      modificarNombre(agenda, p);
      break;
    case '3':
      modificarDireccion(agenda, p);
      break;
    case '4':
      modificarTelefono(agenda, p);
      break;
    case '5':
      borrarPersona(agenda, p);
      break;
    default:
      cout << endl << "opción no válida" << endl;
  }
  cout << endl;
}

void submenuSeleccionarPersona(Agenda *agenda, List<Persona> *ps) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> seleccionar persona" << endl << endl;
  
  cout << "opciones:" << endl;
  ListIterator<Persona> *iter = ps->getIterator();
  while (iter->hasNext()) {
    cout << *iter->next() << endl;
  }
  cout << "0: salir" << endl << endl;
  delete(iter);
  
  cout << "opción: ";
  int i = -1;
  cin >> i;
  if (cin.fail()) {
    cout << "opción no válida" << endl;
    cin.clear();
  } else {
    Persona *p;
    if (agenda->tryGetById(i, p)) {
      submenuModificarOBorrarPersona(agenda, p);
    } else if (i != 0) {
      cout << "opción no válida" << endl;
    }
  }
}

void buscarPersonaPorApellido(Agenda *agenda) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> buscar persona por apellido" << endl << endl;
  cout << "ingresá el apellido a buscar: ";
  
  clearCin();
  char apellido[30];
  cin.getline(apellido, sizeof(apellido));
  cout << endl;

  List<Persona> *psConApellido;
  if (agenda->tryGetByApellido(apellido, psConApellido)) {
    submenuSeleccionarPersona(agenda, psConApellido);
  } else {
    cout << "no hay personas agendadas con ese apellido" << endl;
  }
}

void buscarPersonaPorTelefono(Agenda *agenda) {
  cout << endl << endl << endl;
  cout << "A G E N D A -> buscar persona por teléfono" << endl << endl;
  cout << "ingresá el teléfono a buscar: ";
  
  clearCin();
  char telefono[20];
  cin.getline(telefono, sizeof(telefono));
  cout << endl;

  List<Persona> *psConTelefono;
  if (agenda->tryGetByTelefono(telefono, psConTelefono)) {
    submenuSeleccionarPersona(agenda, psConTelefono);
  } else {
    cout << "no hay personas agendadas con ese teléfono" << endl;
  }
}

void loopMenuPrincipal(Agenda *agenda) {
  char c;
  while (c != '0') {
    cout << endl << endl << endl;
    cout << "A G E N D A" << endl << endl;
    cout << "opciones:" << endl;
    cout << "1. agregar persona" << endl;
    cout << "2. buscar persona por apellido" << endl;
    cout << "3. buscar persona por teléfono" << endl;
    cout << "0. salir" << endl << endl;
    cout << "opción: ";
    cin >> c;
    
    switch(c) {
      case '0':
        break;
      case '1':
        agregarPersona(agenda);
        break;
      case '2':
        buscarPersonaPorApellido(agenda);
        break;
      case '3':
        buscarPersonaPorTelefono(agenda);
        break;
      default:
        cout << endl << "opción no válida" << endl;
    }
  }
  cout << endl;
}

int main(int argc, char const *argv[]) {
  Agenda *agenda = new Agenda();
  
  // datos de prueba
  agenda->add(new Persona(agenda->getNewId(), "Alvaro", "Alvarez", "78 Nº 87", "15-5678-9012"));
  agenda->add(new Persona(agenda->getNewId(), "Benita", "Benítez", "1516 Nº 1615", "2345-6789"));
  agenda->add(new Persona(agenda->getNewId(), "Benito", "Benítez", "1516 Nº 1615", "2345-6789"));
  agenda->add(new Persona(agenda->getNewId(), "Fernanda", "Fernández", "56 Nº 65", "3456-7890"));
  agenda->add(new Persona(agenda->getNewId(), "Gonzalo", "González", "910 Nº 10900", "15-4567-8901"));
  agenda->add(new Persona(agenda->getNewId(), "Martín", "Martínez", "1112 Nº 1211", "3456-7890"));
  agenda->add(new Persona(agenda->getNewId(), "Pedro", "Pérez", "34 Nº 43", "15-6789-0123"));
  agenda->add(new Persona(agenda->getNewId(), "Rodrigo", "Rodríguez", "78 Nº 87", "15-8901-2345"));
  agenda->add(new Persona(agenda->getNewId(), "Sancho", "Sánchez", "78 Nº 87", "15-5678-9012"));

  loopMenuPrincipal(agenda);
  
  delete(agenda);
}

