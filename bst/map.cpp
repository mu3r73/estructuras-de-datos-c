#pragma once

#include "map.hpp"
#include "list.hpp"


template<typename TKey, typename TValue>
MNode<TKey, TValue>::~MNode() {
  if (this->value != nullptr) {
    delete(this->value);
    this->value = 0;
  }
}

template<typename TKey, typename TValue>
Map<TKey, TValue>::Map() {
  this->root = nullptr;
  this->length = 0;
}

template<typename TKey, typename TValue>
Map<TKey, TValue>::~Map() {
  MNode<TKey, TValue> *max;
  while (!this->isEmpty()) {
    max = this->getMax(this->root);
    this->tryRemove(max->key);
  }
}

template<typename TKey, typename TValue>
bool Map<TKey, TValue>::isEmpty() {
  return (this->length == 0);
}

template<typename TKey, typename TValue>
void Map<TKey, TValue>::addOrUpdate(TKey key, TValue *value) {
  MNode<TKey, TValue> *res = this->addOrUpdateRec(this->root, key, value);
  if (this->root == nullptr) {
    this->root = res;
  }
}

template<typename TKey, typename TValue>
MNode<TKey, TValue>* Map<TKey, TValue>::addOrUpdateRec(MNode<TKey, TValue> *node, TKey key, TValue *value) {
  if (node == nullptr) {
    node = new MNode<TKey, TValue>();
    node->key = key;
    node->value = value;
    this->length++;
  } else if (key < node->key) {
    node->left = this->addOrUpdateRec(node->left, key, value);
  } else if (key > node->key) {
    node->right = this->addOrUpdateRec(node->right, key, value);
    return node;
  } else {
    // node->key == key
    node->value = value;
  }
  return node;
}

template<typename TKey, typename TValue>
bool Map<TKey, TValue>::tryGet(TKey key, TValue *&value) {
  MNode<TKey, TValue> *node = this->tryGetRec(root, key);
  if (node != nullptr) {
    value = node->value;
    return true;
  } else {
    return false;
  }
}

template<typename TKey, typename TValue>
MNode<TKey, TValue>* Map<TKey, TValue>::tryGetRec(MNode<TKey, TValue> *node, TKey key) {
  if (node == nullptr) {
    return nullptr;
  }
  if (key < node->key) {
    return this->tryGetRec(node->left, key);
  }
  if (key > node->key) {
    return this->tryGetRec(node->right, key);
  }
  // key == node->key
  return node;
}

template<typename TKey, typename TValue>
bool Map<TKey, TValue>::tryRemove(TKey key) {
  MNode<TKey, TValue> *node = this->tryRemoveRec(root, key);
  return (node != nullptr);
}

template<typename TKey, typename TValue>
MNode<TKey, TValue>* Map<TKey, TValue>::tryRemoveRec(MNode<TKey, TValue> *node, TKey key) {
  if (node == nullptr) {
    return nullptr;
  }
  if (key < node->key) {
    node->left = this->tryRemoveRec(node->left, key);
    return node;
  }
  if (key > node->key) {
    node->right = this->tryRemoveRec(node->right, key);
    return node;
  }
  // key == node->key
  MNode<TKey, TValue> *newNode = this->rebuildSubtree(node->left, node->right);
  bool isRoot = (node == this->root);
  delete(node);
  this->length--;
  if (isRoot) {
    this->root = newNode;
  }
  return newNode;
}

template<typename TKey, typename TValue>
int Map<TKey, TValue>::getLength() {
  return this->length;
}

template<typename TKey, typename TValue>
MNode<TKey, TValue>* Map<TKey, TValue>::rebuildSubtree(MNode<TKey, TValue> *left, MNode<TKey, TValue> *right) {
  if ((left == nullptr) && (right == nullptr)) {
    return nullptr;
  }
  if (left == nullptr) {
    return right;
  }
  if (right == nullptr) {
    return left;
  }
  // ni left ni right son nullptr
  MNode<TKey, TValue> *newNode = this->getMax(left);
  newNode->left = this->tryRemoveRec(left, newNode->key);
  newNode->right = right;
  return newNode;
}

template<typename TKey, typename TValue>
MNode<TKey, TValue>* Map<TKey, TValue>::getMax(MNode<TKey, TValue> *node) {
  if (node->right != nullptr) {
    return this->getMax(node->right);
  }
  // node->right es vacío
  return node;
}

template<typename TKey, typename TValue>
List<TKey>* Map<TKey, TValue>::keys() {
  // this new causes a leak
  List<TKey> *res = new List<TKey>();
  this->keysRec(res, this->root);
  return res;
}

template<typename TKey, typename TValue>
void Map<TKey, TValue>::keysRec(List<TKey> *res, MNode<TKey, TValue> *node) {
  if (node == nullptr) {
    return;
  }
  // no es vacío
  this->keysRec(res, node->left);
  res->append(new TKey(node->key));
  this->keysRec(res, node->right);
}

