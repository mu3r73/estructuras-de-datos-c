#pragma once

template<typename T>
class Node {
private:
  T value;
  Node<T>* next;

public:
  Node(const T value);
  T get_value();
  void set_value(T value);
  Node<T>* get_next();
  void set_next(Node<T>* next);
};

