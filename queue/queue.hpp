#pragma once

#include "node.hpp"

template<typename T>
class Queue {
private:
  Node<T>* first;
  Node<T>* last;
  int len;

public:
  Queue();
  ~Queue();
  int length();
  bool is_empty();
  void enqueue(T value);
  void dequeue();
  T top();
};

