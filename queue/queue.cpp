#include "queue.hpp"
#include "node.cpp"

#include <stdexcept>
using namespace std;


template<typename T>
Queue<T>::Queue() {
  this->first = nullptr;
  this->last = nullptr;
  this->len = 0;
}

template<typename T>
Queue<T>::~Queue() {
  while (this->first != nullptr) {
    this->dequeue();
  }
}

template<typename T>
int Queue<T>::length() {
  return this->len;
}

template<typename T>
bool Queue<T>::is_empty() {
  return this->len == 0;
}

template<typename T>
void Queue<T>::enqueue(T value) {
  Node<T>* new_node = new Node<T>(value);
  if (this->is_empty()) {
    this->first = new_node;
  } else {
    this->last->set_next(new_node);
  }
  this->last = new_node;
  this->len++;
}

template<typename T>
void Queue<T>::dequeue() {
  if (this->is_empty()) {
    throw runtime_error{"empty queue"};
  }
  Node<T>* second = this->first->get_next();
  delete(this->first);
  this->first = second;
  this->len--;
}

template<typename T>
T Queue<T>::top() {
  if (this->is_empty()) {
    throw runtime_error{"empty queue"};
  }
  return this->first->get_value();
}

