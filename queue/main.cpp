#include "queue.cpp"

#include <iostream>
using namespace std;


void test_1() {
  cout << "creating q" << endl;
  Queue<int>* q = new Queue<int>();
  cout << "length: " << q->length() << endl << endl;

  cout << "enqueuing 5" << endl;
  q->enqueue(5);
  cout << "top: " << q->top() << endl;
  cout << "length: " << q->length() << endl << endl;

  cout << "enqueuing 3" << endl;
  q->enqueue(3);
  cout << "top: " << q->top() << endl;
  cout << "length: " << q->length() << endl << endl;

  cout << "dequeing" << endl;
  q->dequeue();
  cout << "top: " << q->top() << endl;
  cout << "length: " << q->length() << endl << endl;

  delete(q);
 }


void test_2() {
  cout << "creating q" << endl;
  Queue<string>* q = new Queue<string>();
  cout << "length: " << q->length() << endl << endl;

  cout << "enqueuing 'a'" << endl;
  q->enqueue("hello");
  cout << "top: " << q->top() << endl;
  cout << "length: " << q->length() << endl << endl;

  cout << "enqueuing 'b'" << endl;
  q->enqueue("goodbye");
  cout << "top: " << q->top() << endl;
  cout << "length: " << q->length() << endl << endl;

  cout << "dequeing" << endl;
  q->dequeue();
  cout << "top: " << q->top() << endl;
  cout << "length: " << q->length() << endl << endl;

  delete(q);
 }


int main(int argc, char const *argv[]) {
  try {
    test_1();
    test_2();
  } catch (runtime_error e) {
    cout << e.what() << endl;
  }

  return 0;
}

