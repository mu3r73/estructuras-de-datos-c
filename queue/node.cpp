#include "node.hpp"

template<typename T>
Node<T>::Node(const T value) {
  this->value = value;
  this->next = nullptr;
}

template<typename T>
T Node<T>::get_value() {
  return this->value;
}

template<typename T>
void Node<T>::set_value(const T value) {
  this->value = value;
}

template<typename T>
Node<T>* Node<T>::get_next() {
  return this->next;
}

template<typename T>
void Node<T>::set_next(Node<T>* next) {
  this->next = next;
}

