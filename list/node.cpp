#include "node.hpp"

template<typename T>
Node<T>::Node(const T value) {
  this->value = value;
  this->prev = nullptr;
  this->next = nullptr;
}

template<typename T>
T Node<T>::getValue() {
  return this->value;
}

template<typename T>
void Node<T>::setValue(const T value) {
  this->value = value;
}

template<typename T>
Node<T>* Node<T>::getPrev() {
  return this->prev;
}

template<typename T>
void Node<T>::setPrev(Node<T>* prev) {
  this->prev = prev;
}
template<typename T>
Node<T>* Node<T>::getNext() {
  return this->next;
}

template<typename T>
void Node<T>::setNext(Node<T>* next) {
  this->next = next;
}

