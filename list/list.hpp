#pragma once

#include "node.hpp"

#include <string>

// lista doblemente enlazada genérica
template<typename T>
class List {
private:
  Node<T>* first;
  Node<T>* last;
  int len;

public:
  // constructor - crea una lista vacía
  List();
  // destructor - libera la memoria al borrar la lista
  ~List();
  // indica si la lista está vacía
  bool isEmptyList();
  // agrega un elemento a la lista en la posición indicada
  // (si la lista no llega a esa posición, lo agreag al final)
  void add(int pos, T value);
  // devuelve el elemento de la lista en la posición indicada
  T value(int pos);
  // indica si existe la posición indicada en la lista
  bool existsValue(int pos);
  // elimina el elemento en la posición indicada de la lista
  void deleteValue(int pos);
  // devuelve la longitud de la lista
  int length();
  // destruye la lista, liberando memoria
  void destroy();

private:
  // localiza la posición indicada en la lista
  // (o el fin de la lista si la posición no existe)
  Node<T>* locatePos(int pos);
};

