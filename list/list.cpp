#include "list.hpp"
#include "node.cpp"

#include <stdexcept>
using namespace std;


template<typename T>
List<T>::List() {
  this->first = nullptr;
  this->last = nullptr;
  this->len = 0;
}

template<typename T>
List<T>::~List() {
  while (this->first != nullptr) {
    this->deleteValue(0);
  }
}

template<typename T>
bool List<T>::isEmptyList() {
  return this->len == 0;
}

template<typename T>
void List<T>::add(int pos, T value) {
  Node<T>* node = new Node<T>(value);
  if (this->isEmptyList()) {
    this->first = node;
    this->last = node;
  } else {
    Node<T>* insertionPoint = this->locatePos(pos);
    if (insertionPoint == this->first) {
      this->first = node;
    }
    if (insertionPoint == nullptr) {
      node->setPrev(this->last);
      this->last->setNext(node);
      this->last = node;
    } else {
      node->setPrev(insertionPoint->getPrev());
      insertionPoint->getPrev()->setNext(node);
      insertionPoint->setPrev(node); 
    }
    node->setNext(insertionPoint);
  }
  this->len++;
}

template<typename T>
T List<T>::value(int pos) {
  if (this->isEmptyList()) {
    throw runtime_error{"lista vacía"};
  }
  Node<T>* node = this->locatePos(pos);
  if (node == nullptr) {
    throw runtime_error("no existe la posición en la lista");
  }
  return node->getValue();
}

template<typename T>
bool List<T>::existsValue(int pos) {
  if (this->isEmptyList()) {
    throw runtime_error{"lista vacía"};
  }
  return this->locatePos(pos) != nullptr;
}

template<typename T>
void List<T>::deleteValue(int pos) {
  if (this->isEmptyList()) {
    throw runtime_error{"lista vacía"};
  }
  Node<T>* node = this->locatePos(pos);
  if (node == nullptr) {
    throw runtime_error("no existe la posición en la lista");
  }
  if (node == this->first) {
    this->first = node->getNext();
  } else {
    node->getPrev()->setNext(node->getNext());
  }
  if (node == this->last) {
    this->last = node->getPrev();
  } else {
    node->getNext()->setPrev(node->getPrev());
  }
  delete(node);
  this->len--;
}

template<typename T>
int List<T>::length() {
  return this->len;
}

template<typename T>
void List<T>::destroy() {
  delete(this);
}

template<typename T>
Node<T>* List<T>::locatePos(int pos) {
  if (pos < 0) {
    throw runtime_error{"posición no válida"};
  }
  int index = 0;
  Node<T>* node = this->first;
  while ((index != pos) && (node != nullptr)) {
    node = node->getNext();
    index++;
  }
  return node;
}

