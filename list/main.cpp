#include "list.cpp"

#include <iostream>
using namespace std;


void mostrarLista(List<int> *l) {
  for (int pos = 0; pos < l->length(); pos++) {
    if (pos > 0) {
      cout << ", ";
    }
    cout << l->value(pos);
  }
  cout << endl;
}


void agregarYMostrar(List<int> *l, int pos, int value) {
  cout << "agregando " << value << " en la posición " << pos << ":" << endl;
  l->add(pos, value);
  cout << "lista: ";
  mostrarLista(l);
  cout << endl;
}


void borrarYMostrar(List<int> *l, int pos) {
  cout << "borrando el elemento en la posición " << pos << ":" << endl;
  l->deleteValue(pos);
  cout << "lista: ";
  mostrarLista(l);
  cout << endl;
}


int main(int argc, char const *argv[]) {
  try {
    cout << "creando lista..." << endl;
    List<int>* l = new List<int>;

    agregarYMostrar(l, 0, 3);
    agregarYMostrar(l, 1, 4);
    agregarYMostrar(l, 10, 5);
    agregarYMostrar(l, 2, 10);
    agregarYMostrar(l, 3, 12);
    agregarYMostrar(l, 4, 41);
    agregarYMostrar(l, 5, 20);

    borrarYMostrar(l, 3);

    cout << "liberando memoria..." << endl;
    l->destroy();
  } catch (runtime_error e) {
    cout << e.what() << endl;
  }

  return 0;
}

