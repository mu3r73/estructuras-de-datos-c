#pragma once

// nodo para una pila o cola genérica
template<typename T>
class Node {
private:
  T value;
  Node<T>* prev;
  Node<T>* next;

public:
  // constructor - crea un nuevo nodo con el valor indicado
  Node(const T value);
  // retorna el valor del nodo
  T getValue();
  // asigna el valor indicado al nodo
  void setValue(T value);
  // retorna un puntero al nodo previo
  Node<T>* getPrev();
  // asigna un puntero al nodo previo
  void setPrev(Node<T>* next);
  // retorna un puntero al siguiente nodo
  Node<T>* getNext();
  // asigna un puntero al siguiente nodo
  void setNext(Node<T>* next);
};

