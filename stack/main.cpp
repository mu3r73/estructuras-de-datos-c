#include "stack.cpp"

#include <iostream>
using namespace std;


Stack<int>* crearPila(const int nums[], size_t cant) {
  Stack<int>* s = new Stack<int>();
  for (size_t i = 0; i < cant; i++) {
    s->push(nums[i]);
  }
  return s;
}


void sacarDeLaPila(Stack<int>* s, int num) {
  if (!s->isEmpty()) {
    int top = s->top();
    s->pop();
    if (num != top) {
      sacarDeLaPila(s, num);
      s->push(top);
    }
  }
}


int main(int argc, char const *argv[]) {
  try {
    cout << "creando pila..." << endl;
    int cant = 7;
    int nums[] = {7, 3, 5, 4, 8, 9, 2};
    Stack<int>* s = crearPila(nums, cant);

    cout << "pila después de agregarle datos iniciales:" << endl;
    cout << s->toString() << endl;

    int sacar = 2;
    cout << "eliminando la primera ocurrencia del número " << sacar << "..." << endl;
    sacarDeLaPila(s, sacar);

    cout << "pila después de desapilar el " << sacar << ":" << endl;
    cout << s->toString() << endl;

    cout << "liberando memoria..." << endl;
    s->destroy();
  } catch (runtime_error e) {
    cout << e.what() << endl;
  }

  return 0;
}
