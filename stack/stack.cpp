#include "stack.hpp"
#include "node.cpp"

#include <stdexcept>
#include <sstream>
#include <string>
using namespace std;


template<typename T>
Stack<T>::Stack() {
  this->last = nullptr;
  this->len = 0;
}

template<typename T>
Stack<T>::~Stack() {
  while (this->last != nullptr) {
    this->pop();
  }
}

template<typename T>
bool Stack<T>::isEmpty() {
  return this->len == 0;
}

template<typename T>
void Stack<T>::push(T value) {
  Node<T>* node = new Node<T>(value);
  if (!this->isEmpty()) {
    node->setNext(this->last);
  }
  this->last = node;
  this->len++;
}

template<typename T>
void Stack<T>::pop() {
  if (this->isEmpty()) {
    throw runtime_error{"stack vacía"};
  }
  Node<T>* second = this->last->getNext();
  delete(this->last);
  this->last = second;
  this->len--;
}

template<typename T>
T Stack<T>::top() {
  if (this->isEmpty()) {
    throw runtime_error{"stack vacía"};
  }
  return this->last->getValue();
}

template<typename T>
int Stack<T>::length() {
  return this->len;
}

template<typename T>
void Stack<T>::destroy() {
  delete(this);
}

template<typename T>
string Stack<T>::toString() {
  stringstream res;

  Node<T>* node = this->last;
  while (node != nullptr) {
    res << node->getValue() << endl;
    node = node->getNext();
  }

  return res.str();
}

