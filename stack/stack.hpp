#pragma once

#include "node.hpp"

#include <string>

// pila genérica
template<typename T>
class Stack {
private:
  Node<T>* last;
  int len;

public:
  // constructor - crea una pila vacía
  Stack();
  // destructor - libera la memoria al borrar la pila
  ~Stack();
  // indica si la pila está vacía
  bool isEmpty();
  // agrega un elemento a la pila 
  void push(T value);
  // descarta el primer elemento de la pila
  void pop();
  // devuelve el elemento del tope de la pila
  T top();
  // devuelve la longitud de la pila
  int length();
  // destruye la pila, liberando memoria
  void destroy();
  // devuelve un string con cada elemento de la pila en una línea
  std::string toString();
};

